package com.invoiceservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InvoiceService {

    @Autowired
    private RestTemplate restTemplate;

    public ResponseEntity<String> getUserString(int id){
        String url = "https://ticketcinemafilmsservice.up.railway.app/api/schedules/getSchedulesByFilmId?id=";
        ResponseEntity<String> response = restTemplate
                .getForEntity(url +id, String.class);
        return response;
    }
}
