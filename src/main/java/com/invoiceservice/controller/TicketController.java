package com.invoiceservice.controller;


import com.invoiceservice.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/invoice")
public class TicketController {

    @Autowired
    private InvoiceService service;

    @GetMapping("/{id}")
    public ResponseEntity<String> findScheduleById(@PathVariable("id") int id){
        return service.getUserString(id);
    }
}
